module gitlab.com/beabys/quetzal-go

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/go-chi/chi v1.5.4
	github.com/prometheus/common v0.4.0
	github.com/spf13/viper v1.7.1
)
