package main

import (
	"gitlab.com/beabys/quetzal-go/pkg/app"
	"gitlab.com/beabys/quetzal-go/pkg/app/resources/hello"
	"gitlab.com/beabys/quetzal-go/pkg/config"
	"gitlab.com/beabys/quetzal-go/pkg/router"
)

func main() {
	c := &config.Config{}
	c.LoadConfig()
	var app = &app.App{
		Config: c,
		Router: router.New(),
	}
	// set the resources
	app.SetRoutes(setHandlers())

	// start mux server
	app.Router.Serve(c.App.Port)
}

func setHandlers() []func(*app.App) {
	return []func(*app.App){
		// Here need to add the routes to be handled
		hello.Handler,
	}
}
