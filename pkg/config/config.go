package config

import (
	"log"

	"github.com/spf13/viper"
)

// Config is a struct define configuration for the app and list of DSP
type Config struct {
	App       ApplicationConfig    `mapstructure:"application"`
	Databases map[string]Databases `mapstructure:"databases"`
}

// ApplicationConfig is a struct to define configurations for the app
type ApplicationConfig struct {
	Port           int    `mapstructure:"port"`
	TmpFolder      string `mapstructure:"tmp-folder"`
	BatchToProcess int    `mapstructure:"batch-to-process"`
	Workers        int    `mapstructure:"workers-to-process"`
}

// Databases is a struct to define configurations for the Databases
type Databases struct {
	Username string `mapstructure:"username"`
	Password string `mapstructure:"password"`
	Host     string `mapstructure:"host"`
	Port     int    `mapstructure:"port"`
	Database string `mapstructure:"database"`
	Logs     bool   `mapstructure:"logs"`
}

// LoadConfig is a function to load the configuration, stored on the config files
func (c *Config) LoadConfig() {

	// Enable VIPER to read Environment Variables
	viper.AutomaticEnv()

	// set config file name
	viper.SetConfigName("config")

	// set path of the config
	viper.AddConfigPath("./")

	viper.SetConfigType("yaml")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Fail to load configs: %s", err)
	}

	if err := viper.Unmarshal(&c); err != nil {
		log.Fatalf("Unable to decode application configs: %s", err)
	}
	port := viper.GetInt("APPLICATION_PORT")
	if port == 0 {
		port = 80
	}
	tmpFolder := viper.GetString("TMP_FOLDER")
	c.App.Port = port
	c.App.TmpFolder = tmpFolder
}
