package router

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/prometheus/common/log"
)

// Router is a struct to define a mux router
type Router struct {
	*chi.Mux
}

// New return Router Type
func New() *Router {
	return &Router{chi.NewRouter()}
}

// Serve app on port
func (r *Router) Serve(port int) {
	log.Infof("Starting Server on port: %v", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", port), r))
}
