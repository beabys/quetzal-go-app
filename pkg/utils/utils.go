package utils

import (
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"time"
)

// Response is a struct response
type Response struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

// Empty is a empty struct response
type Empty struct{}

// ResponseJSON return an error response with status code
func ResponseJSON(w http.ResponseWriter, statusCode int, message string, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	if data == nil {
		data = Empty{}
	}
	var response = Response{
		Status:  statusCode,
		Data:    data,
		Message: message,
	}
	responseWriter(w, &response)
}

// RespondJSON return a response status ok with data
func responseWriter(w http.ResponseWriter, response *Response) {
	responseData, err := json.Marshal(response)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Something went wrong :(")
	}
	fmt.Fprintf(w, string(responseData))
}

// RespondNoContent return an empty content with the same status code
func RespondNoContent(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNoContent)
}

// Time transform string validating the layouts
func Time(value string) (time.Time, error) {
	var defaultTime time.Time
	layout := "2006-01-02T15:04:05-07:00"
	matchPlus, _ := regexp.MatchString("+", value)
	if matchPlus {
		layout = "2006-01-02T15:04:05+07:00"
	}
	matchZ, _ := regexp.MatchString("Z", value)
	if matchZ {
		layout = "2006-01-02T15:04:05Z"
	}
	result, err := time.Parse(layout, value)
	if err != nil {
		return defaultTime, err
	}
	return result, nil
}

// FindInSlice function to find data inside one slice
// if it's success will return true
func FindInSlice(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}
