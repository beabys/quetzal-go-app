package hello

import (
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/beabys/quetzal-go/pkg/app"
	"gitlab.com/beabys/quetzal-go/pkg/app/middleware"
	"gitlab.com/beabys/quetzal-go/pkg/utils"
)

type resource struct {
	*app.App
}

// Handler returns routes of handlers functions
func Handler(app *app.App) {
	r := app.Router
	rsrc := resource{app}
	// Post with middleware only for this route
	// r.With(middleware.Foo).Get("/foo", rsrc.getReport)
	r.Route("/hello", func(r chi.Router) {
		r.Use(middleware.Example)
		r.Get("/", rsrc.getHello)
		r.Post("/", rsrc.postHello)
	})
}

func (rsrc *resource) getHello(w http.ResponseWriter, r *http.Request) {
	utils.ResponseJSON(w, http.StatusOK, "Hello World!...get", r.URL.Query())
	return
}

func (rsrc *resource) postHello(w http.ResponseWriter, r *http.Request) {
	// get the context
	ctx := r.Context()
	parameters, _ := ctx.Value("parameters").(string)
	utils.ResponseJSON(w, http.StatusOK, "Hello World!...Post", parameters)
	return
}
