package middleware

import (
	"context"
	"net/http"
)

// ValidateRequest is a middleware to validate the request to report
func Example(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		ctx = context.WithValue(ctx, "parameters", "passing parameter from  middleware")
		next.ServeHTTP(w, r.WithContext(ctx))
		return
	})
}
