package middleware

import (
	"net/http"

	"gitlab.com/beabys/quetzal-go/pkg/utils"
)

// NotFound handler middleware
func NotFound(w http.ResponseWriter, r *http.Request) {
	utils.ResponseJSON(w, http.StatusNotFound, "Not Found", nil)
	return
}
