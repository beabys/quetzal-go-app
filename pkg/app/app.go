package app

import (
	"github.com/go-chi/chi/middleware"
	middlewares "gitlab.com/beabys/quetzal-go/pkg/app/middleware"
	"gitlab.com/beabys/quetzal-go/pkg/config"
	"gitlab.com/beabys/quetzal-go/pkg/router"
)

var routes []func(*App)

type App struct {
	Config *config.Config
	Router *router.Router
	// Database *database.Connection
}

// func to set the routes of the app
func (app *App) SetRoutes(handlers []func(*App)) {
	//stack of middlewares from chi
	app.Router.Use(middleware.RequestID)
	app.Router.Use(middleware.RealIP)
	app.Router.Use(middleware.Logger)
	app.Router.Use(middleware.Recoverer)
	app.Router.NotFound(middlewares.NotFound)
	// bind our handlers to the app
	app.BootRoutes(handlers)
}

// BootRoutes append routes setted on each handle function
func (app *App) BootRoutes(routes []func(*App)) {
	// routes and middleware are handled inside the handler function
	for _, f := range routes {
		f(app)
	}
}
